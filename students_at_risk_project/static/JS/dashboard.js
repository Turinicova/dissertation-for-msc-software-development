var atRiskGraphData;
var courseData;

//loads course graph on pageload
$(function () {
    $.ajax({
        url: "/studentsatrisk/get_dashboard_data",
        method: "GET",
        data: {
            option: "course_graph",
        },
        beforeSend: function () {

        },
        success: function (response) {
            courseData = response
            list(response)
            indexFirstGraph = 0
            loadCourseGraphData(courseData, indexFirstGraph)
        },
        error: function (response) {
            console.log("FAIL");
            console.log(response);
        }
    });

    //function to prepare data for course graph  and make the graph
    function loadCourseGraphData(courseData, index){
        title = courseData.all_courses_list
        title_graph1 = title['all_courses_list']
        title_graph1 = title_graph1[index]

        p_data = courseData.pres_data
        p_graph1 = p_data['data']
        p_graph1 = p_graph1[index]

        l_data = courseData.link_data
        l_graph1 = l_data['data']
        l_graph1 = l_graph1[index]

        y_data = courseData.yacrs_data
        y_graph1 = y_data['data']
        y_graph1 = y_graph1[index]

        a_data = courseData.addmat_data
        a_graph1 = a_data['data']
        a_graph1 = a_graph1[index]

        generateGraphCourse(title_graph1, p_graph1, l_graph1, y_graph1, a_graph1)
    }

    // function that sends all the dropdown menu items from ajax to html
    function list(courseData){
        all_courses_list = courseData.all_courses_list.all_courses_list
        var courseList = document.getElementById('all-courses');
        for (var i = 0; i < all_courses_list.length; i++){
            courseList.innerHTML += '<li class="course-list-dropdown" id="item-'+i+'">'+ all_courses_list[i] + '</li>';
        }
    };

    // event handlers for graph course
	//show menu when user clicks on dropdown button
    $('body').on('click', '.dropbtn', function(){
        document.getElementById('coursesDropdown').classList.toggle("show");
    });
	
	//change button label based on item selection
    $('body').on('click', '.course-list-dropdown', function(){
        var currId = $(this).attr('id').split('-')[1];
        loadCourseGraphData(courseData, currId);
        $("#course-name-button").html('Course selection: ' + $(this).html());
    });
	
	//search function for the elements of the dropdown menu, based on every added letter
    $('body').on('keyup', '#searchInput', function() {
        var input = document.getElementById('searchInput');
        var filter = input.value.toLowerCase();
        var all_items = document.getElementById('all-courses');
        var li = all_items.getElementsByTagName('li');

        for (var i = 0; i < li.length; i++) {
            text_match = li[i].textContent || li[i].innerText;
            if (text_match.toLowerCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    });

    // Default ajax for graph data showing students at risk.. loads on pageload
    $.ajax({
        url: "/studentsatrisk/get_dashboard_data",
        method: "GET",
        data: {
            option: "time_graph",
        },
        beforeSend: function () {

        },
        success: function (response) {
            atRiskGraphData = response;
            generateGraphTime(response['students_at_risk_one_week']);
            $('.at-risk-buttons').removeClass('hidden');
        },
        error: function (response) {
            console.log("FAIL");
            console.log(response);
        }
    });

    // event handler for students at risk graph 1 week button
    $('body').on('click', '#at-risk-1-week', function(){
        generateGraphTime(atRiskGraphData['students_at_risk_one_week']);
        $('.at-risk-button').removeClass('active')
        $(this).addClass('active');
    });

    // event handler for students at risk graph 3 months button
    $('body').on('click', '#at-risk-3-months', function(){
        generateGraphTime(atRiskGraphData['students_at_risk_three_months']);
        $('.at-risk-button').removeClass('active')
        $(this).addClass('active');
    });

     // event handler for students at risk graph 1 year button
    $('body').on('click', '#at-risk-year', function(){
        generateGraphTime(atRiskGraphData['students_at_risk_year']);
        $('.at-risk-button').removeClass('active')
        $(this).addClass('active');
    });
});

//function that specifies graph structure and display, as well as data display for course graph
//based on Highcharts.com graphs documentation
function generateGraphCourse(title, pres_data, link_data, yacrs_data, addmat_data) {
    Highcharts.chart('container_graph_course', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Per Course Engagement over the last Week'
        },
        subtitle: {
            text: 'Course: ' + title
        },
        xAxis: {
            categories: ['Presentations', 'Links','YACRS', 'Additional Material'],
            crosshair: true
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Number of Students'
            }
        },
        tooltip: {

            headerFormat: '<span style="font-size:10px"><strong>{point.x}</span></strong><br/><table>',
            pointFormat: '<span style="color:{series.color};padding:0">{series.name}</span>: <b>{point.y}</b> <br/>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Clicked',
            data: [pres_data[0], link_data[0], yacrs_data[0], addmat_data[0]]
        }, {
            name: 'Not Clicked',
            data: [pres_data[1], link_data[1], yacrs_data[1], addmat_data[1]]
        }
        ]
    });
}

//function that specifies the type of graph and data to display for graph time
//based on Highcharts.com graphs documentation
function generateGraphTime(data) {
    Highcharts.setOptions({
        time: {
            timezone: 'Europe/London'
        }
    });
    Highcharts.chart('container_graph_time', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'At Risk Students over Time'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'date',
            categories: data.dates,

            labels: {
                formatter: function () {
                    return moment(this.value).format("YYYY-MM-DD");
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Students'
            }
        },
        tooltip: {
            crosshairs: true,
            headerFormat: '<span style="font-size:10px"><strong>{point.key}</span></strong><br/><table>',
            pointFormat: '<span style="color:{series.color};padding:0">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            area: {
                stacking: 'normal',
                lineColor: '#666666',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#666666',
                }
            }
        },
        series: [{
            name: 'Students at risk',
            data: data.numbers
        }
        ]
    });
}