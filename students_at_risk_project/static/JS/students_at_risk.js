var tableData;
var buttonData;

//load all at-risk students specified in the view..at pageload
$(function () {
    $.ajax({
        url: "/studentsatrisk/students_at_risk_display",
        type: "GET",
        data: {
             option : "display_data"
        },
        success : function(response) {
            tableData = response;
            loadTable(response['student_data_risk'])
        },
        error: function (response) {
            console.log("FAIL");
            console.log(response);
        }
    });

    // event handlers for buttons to filter table data
    $('body').on("click", "#button-all-students-display", function(){
        loadTable(tableData['student_data_all']);
    });

    $('body').on("click", "#button-only-at-risk-display", function(){
        loadTable(tableData['student_data_risk']);
    });
	
	//event handler for buttons to flag students as at risk
    $('body').on("click", ".students-at-risk-buttons-list", function(){
        var this_id = $(this).attr("id");
        var $thisElement = $(this);
		//sends request to view where the respective model gets updated
        $.ajax({
            url: "/studentsatrisk/at_risk_status_update",
            type: "GET",
            data: {
                 option : 'at_risk_update_status',
                 SID: this_id.split("-")[0],
                 course: this_id.split("-")[1],
            },
            success : function(response) {
                buttonData = response;
                if(!response.wrong_request){
					//changes displayed label of button based on the risk status
                    $thisElement.html(response.at_risk ? "At risk" : "Not at risk");
                }
            },
            error: function (response) {
                console.log("FAIL");
                console.log(response);
            }
        });
    });
	
	//search filer for the table
    $('body').on('keyup', "#search-input-table", function(){
        var input = $(this).val().toLowerCase();
        $('#student-data-filter tr').filter(function(){
           $(this).toggle($(this).text().toLowerCase().indexOf(input) > -1)
        });
    });
	
	//function that specifies column data of the tables
    function loadTable(data){

        let data_list = data[0];
        let cols = data_list.length;
        let rows = data_list[0].length;
        let id_data = data_list[0];
        let name_data = data_list[1];
        let level_data = data_list[2];
        let course_data = data_list[3];
        let date_data = data_list[4];
        let at_risk_data = data_list[5];
        let self_flagged = data_list[6];

        var tBodyHtml = "";

        for (var i = 0; i < rows; i++){
            tBodyHtml +=
                '<tr><td>' + id_data[i] + '</td>'+
                '<td>' + name_data[i] + '</td>'+
                '<td>' + level_data[i] + '</td>'+
                '<td>' + course_data[i] + '</td>'+
                '<td>' + date_data[i] + '</td>'+
                '<td><button class="btn btn-secondary students-at-risk-buttons-list" id="'+ (id_data[i] + "-" + course_data[i].split(" ")[0]) +'">' + (at_risk_data[i] ? "At risk" : "Not at risk") + '</button></td>'+
                '<td>' + (self_flagged[i] ? "Self-flagged!" : "") + '</td></tr>'
            ;
        }
		//attaches the prepared data to the body of the table
        $('#student-data-filter').html(tBodyHtml);

    };

});

