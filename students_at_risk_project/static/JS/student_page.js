//function that allows self-flagging - sends request from the html page to view with specifics
$(function () {
    $('body').on("click", "#help-button", function(){
    var select = $("#dropdown-self-flag").val();
        console.log(select)
        $.ajax({
            url: "/studentsatrisk/self_flagging",
            type: "GET",
            data: {
                 option : 'self-flag-request',
                 course : select,
            },
            success : function(response) {
                if(!response.wrong_request){
                    data = response['at_risk_courses'];
                    risk_courses = data['courses_list'];
                    risk_dates = data['dates'];
                    var result = [];
                    for (i=0; i< risk_courses.length; i++){
                        result+="<li>⊗ "+risk_courses[i]+ " - flagged on " +risk_dates[i]+"</li>";
                        console.log(result)
                     }
					 //replace the html element with updated list of flagged courses after self-flagging
                     $('#flagged-courses-list-student-page').html("<ul>"+result+"</ul>")
                     $('#flag-success-message').append("<p>You have been flagged as at risk in "+ select +"</p>");
                }
            },
            error: function (response) {
                console.log("FAIL");
                console.log(response);
            }
        });
    });
 });