"""students_at_risk_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from studentsatrisk import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^instructor_index/$', views.instructor_index, name='instructor_index'),
    url(r'^students_at_risk/$', views.students_at_risk, name='students_at_risk'),
    url(r'^students_at_risk_display/$', views.students_at_risk_display, name='students_at_risk_display'),
    url(r'^at_risk_status_update/$', views.at_risk_status_update, name='at_risk_status_update'),
    url(r'^mypage_student/$', views.student_page, name='student_page'),
    url(r'^self_flagging/$', views.self_flagging, name='self_flagging'),
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    url(r'^get_dashboard_data/$', views.get_dashboard_data, name='get_dashboard_data'),
]

