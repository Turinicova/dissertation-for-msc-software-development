from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth import REDIRECT_FIELD_NAME


# Decorator to check if the user is an instructor - if not, take him to login
def instructor_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url='login'):
    instructor_decorator = user_passes_test(lambda i: i.is_active and i.is_instructor,
                                            login_url=login_url,
                                            redirect_field_name=redirect_field_name)
    if function:
        return instructor_decorator(function)
    return instructor_decorator


# Decorator to check if the user is a student
def student_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url='login'):
    student_decorator = user_passes_test(lambda s: s.is_active and s.is_student,
                                         login_url=login_url,
                                         redirect_field_name=redirect_field_name)
    if function:
        return student_decorator(function)
    return student_decorator
