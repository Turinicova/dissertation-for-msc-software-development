from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView, TemplateView
import datetime as dt
import pandas as pd
import os
from django.conf import settings
from studentsatrisk.decorators import instructor_required, student_required
from studentsatrisk.models import User, School, Level, Course, Instructor,\
    CourseCurrent, Student, StudentCourse,\
    Presentation, PresStudent, Link, LinkStudent, \
    YACRS, YACRSStudent, AddMaterial, AddMatStudent
from studentsatrisk.forms import StudentForm, InstructorForm


# the view also updates the database with at risk information
# this page is for access to the web app
# takes an authenticated user to where he belongs, i.e. relevant homepage
def index(request):
    # check if there are any objects at all
    student_course_exists = StudentCourse.objects.filter()

    if len(student_course_exists) > 0:
        today = dt.date.today()
        # range of 2 weeks for no activity to have a student marked as at risk
        last_update = today - dt.timedelta(days=14)
        two_weeks_range = [last_update, today]
        all_students_courses = StudentCourse.objects.exclude(date_flagged__range=two_weeks_range)

        # per student per course
        for s in all_students_courses:
            dates_list = []
            pres_student = PresStudent.objects.filter(SID=s.student.SID, course_name=s.course.name)
            link_student = LinkStudent.objects.filter(SID=s.student.SID, course_name=s.course.name)
            yacrs_student = YACRSStudent.objects.filter(SID=s.student.SID, course_name=s.course.name)
            addmat_student = AddMatStudent.objects.filter(student_SID=s.student.SID, course_name=s.course.name)

            for p in pres_student:
                if p.date_opened not in two_weeks_range:
                    dates_list.append(p)
            for l in link_student:
                if l.date_opened not in two_weeks_range:
                    dates_list.append(l)
            for y in yacrs_student:
                if y.date_accessed not in two_weeks_range:
                    dates_list.append(y)
            for a in addmat_student:
                if a.date_opened not in two_weeks_range:
                    dates_list.append(a)

            if len(dates_list) > 0:
                s.at_risk = True
                s.date_flagged = today
                s.save()
            else:
                s.at_risk = False
                s.date_flagged = None
                s.save()

    # upon login take user to their view
    if request.user.is_authenticated:
        if request.user.is_instructor:
            return HttpResponseRedirect(reverse('instructor_index'))
        else:
            return HttpResponseRedirect(reverse('student_page'))

    context_dict = {}
    return render(request, 'studentsatrisk/index.html', context=context_dict)


# student dashboard
@login_required
@student_required
def student_page(request):
    user_name = request.user
    student_name = Student.objects.get(user=user_name)
    student_courses_list = StudentCourse.objects.filter(student=student_name.SID).order_by('course')
    flagged_course_list = student_courses_list.filter(at_risk=True)

    context_dict = {'student_name': student_name.name + " " + student_name.surname,
                    'student_courses_list': student_courses_list,
                    'flagged_course_list': flagged_course_list}
    return render(request, 'studentsatrisk/student_page.html', context=context_dict)


# for student to mark themselves as at risk
def self_flagging(request):
    if request.GET['option'] == 'self-flag-request':
        # get the student and the course he selected in dropdown, mark his as at risk
        user_name = request.user
        student = Student.objects.get(user=user_name)
        course_self_flagged = Course.objects.get(name=request.GET['course'])
        student_course = StudentCourse.objects.get(student=student, course=course_self_flagged)
        student_course.flagged_by_himself = not student_course.flagged_by_himself
        if student_course.flagged_by_himself==True:
            student_course.at_risk = True
            student_course.date_flagged = dt.date.today()
        student_course.save()

        at_risk_courses = {}
        risk_dates = []
        risk_courses = []
        risk_courses_set = StudentCourse.objects.filter(student=student, at_risk=True)
        for course in risk_courses_set:
            risk_courses.append(str(course.course))
            risk_dates.append(str(course.date_flagged))

        at_risk_courses['courses_list'] = risk_courses
        at_risk_courses['dates'] = risk_dates

        return JsonResponse({'at_risk_courses': at_risk_courses,
                             'wrong_request': False})

    return JsonResponse({'wrong_request': True})


@login_required
@instructor_required
def instructor_index(request):
    # has to be name of the instructor who is logged in
    user_name = request.user
    instructor_name = Instructor.objects.get(user=user_name)

    students_at_risk_list = StudentCourse.objects.filter(at_risk=True).order_by('-date_flagged')[:10]
    advisee_list = Student.objects.filter(advisor=instructor_name.IID)

    id_list = []
    risk_status = []

    # an instructor sees his advisees separately
    for advisee in advisee_list:
        # one student is enrolled in multiple courses
        student_and_courses = StudentCourse.objects.filter(student=advisee)
        if len(student_and_courses) >= 1:
            id_list.append(str(student_and_courses[0].student))

        if StudentCourse.objects.filter(at_risk=True, student=advisee).exists():
            risk_status.append('AT RISK!')
        else:
            risk_status.append('doing well :)')

    instructor_courses_list = CourseCurrent.objects.filter(instructor_id=instructor_name.IID)[:10]

    context_dict = {'greeting_name': instructor_name.name + " " + instructor_name.surname,
                    'students_at_risk': students_at_risk_list,
                    'my_advisees': id_list,
                    'risk_status': risk_status,
                    'my_courses': instructor_courses_list,
                    }

    return render(request, 'studentsatrisk/instructor_index.html', context=context_dict)


@login_required
@instructor_required
def students_at_risk(request):
    students_list = []
    students = Student.objects.all()
    for s in students:
        student = StudentCourse.objects.filter(student=s.SID)

        for course_instance in student:
            risk_value = course_instance.at_risk

            students_list.append([str(course_instance), risk_value])

    context_dict = {'students': students_list}
    return render(request, 'studentsatrisk/students_at_risk.html', context=context_dict)


# this is the data that tables in this view actually display
@login_required
@instructor_required
def students_at_risk_display(request):
    if 'option' not in request.GET:
        return HttpResponse("Sorry, something appears to have gone wrong. Try to refresh your page.")

    result = {}
    if request.GET['option'] == 'display_data':
        # students can appear more times in a search as a student is at risk on per course basis, not per SID
        studentsatrisk = StudentCourse.objects.filter(at_risk=True).order_by('-date_flagged')

        all_students = StudentCourse.objects.all().order_by('student')
        result_risk = []
        result_all = []

        student_sid = []
        student_name = []
        student_level = []
        student_risk_course = []
        student_is_at_risk = []
        student_date_flagged = []
        self_flagged_list = []

        # data for students_at_risk option button
        for student_at_risk in studentsatrisk:
            current_student = Student.objects.get(SID=student_at_risk.student.SID)
            student_sid.append(current_student.SID)
            student_name.append(current_student.name + " " + current_student.surname)
            student_level.append(str(current_student.level))
            student_risk_course.append(str(student_at_risk.course))
            student_date_flagged.append(student_at_risk.date_flagged)
            student_is_at_risk.append(True)
            self_flagged_list.append(student_at_risk.flagged_by_himself)
        result_risk.append([student_sid, student_name, student_level, student_risk_course, student_date_flagged, student_is_at_risk, self_flagged_list])

        # here a list result for all students
        student_sid_all = []
        student_name_all = []
        student_level_all = []
        student_risk_course_all = []
        student_date_flagged_all = []
        student_is_at_risk_all = []
        self_flagged_list_all = []

        # data for button displaying all students
        for student in all_students:
            current_student = Student.objects.get(SID=student.student.SID)
            student_sid_all.append(current_student.SID)
            student_name_all.append(current_student.name + " " + current_student.surname)
            student_level_all.append(str(current_student.level))
            student_risk_course_all.append(str(student.course))
            if student.at_risk:
                student_date_flagged_all.append(student.date_flagged)
                student_is_at_risk_all.append(True)
                self_flagged_list_all.append(student.flagged_by_himself)
            else:
                student_date_flagged_all.append('')
                student_is_at_risk_all.append(False)
                self_flagged_list_all.append(False)

        result_all.append([student_sid_all, student_name_all, student_level_all, student_risk_course_all, student_date_flagged_all, student_is_at_risk_all, self_flagged_list_all])
        result['student_data_risk'] = result_risk
        result['student_data_all'] = result_all
    # send to ajax
    return JsonResponse(result)


# this is a function that receives data from ajax and updates risk status based on instructor clicking button
@login_required
@instructor_required
def at_risk_status_update(request):
    if request.GET['option'] == 'at_risk_update_status':
        SID = request.GET['SID']
        course = Course.objects.get(code=request.GET['course'])
        student = StudentCourse.objects.get(student=SID, course=course)
        student.at_risk = not student.at_risk
        if student.at_risk:
            student.date_flagged = dt.date.today()
        else:
            student.date_flagged = None
            student.self_flagged = False
        student.save()

        return JsonResponse({'at_risk': student.at_risk, 'wrong_request': False})

    return JsonResponse({'wrong_request': True})


# prepares data for dahsboard analytics graphs
@login_required
def get_dashboard_data(request):
    if 'option' not in request.GET:
        return HttpResponse("Sorry, something appears to have gone wrong. Try to refresh your page.")

    result = {}
    # data for per course view
    if request.GET['option'] == 'course_graph':

        today = dt.date.today()
        one_week_ago = today - dt.timedelta(days=7)
        one_week_range = [one_week_ago, today]
        all_courses_current = CourseCurrent.objects.all()

        pres_data = []
        link_data = []
        yacrs_data = []
        addmat_data = []
        all_courses_list = []

        for coursecurrent in all_courses_current:
            all_courses_list.append(str(coursecurrent))

            # create list for presentations data
            students_pres_opened_number = len(PresStudent.objects.filter(opened=True, date_opened__range=one_week_range, course_name=coursecurrent.course_name))
            all_pres_not_opened = PresStudent.objects.filter(opened=False, course_name=coursecurrent.course_name)
            students_pres_not_opened_number = len(all_pres_not_opened)
            students_pres_not_opened_ID = []

            for student in all_pres_not_opened:
                if str(student) not in all_pres_not_opened:
                    students_pres_not_opened_ID.append(str(student))

            pres_data.append([students_pres_not_opened_number, students_pres_opened_number, students_pres_not_opened_ID])

            # create list for links data
            students_link_opened_number = len(LinkStudent.objects.filter(opened=True, date_opened__range=one_week_range, course_name=coursecurrent.course_name))
            all_link_not_opened = LinkStudent.objects.filter(opened=False, course_name=coursecurrent.course_name)
            students_link_not_opened_number = len(all_link_not_opened)
            students_link_not_opened_ID = []

            for student in all_link_not_opened:
                if str(student) not in all_link_not_opened:
                    students_link_not_opened_ID.append(str(student))

            link_data.append([students_link_not_opened_number, students_link_opened_number, students_link_not_opened_ID])

            # create list for addmat data
            students_addmat_opened_number = len(AddMatStudent.objects.filter(opened=True, date_opened__range=one_week_range, course_name=coursecurrent.course_name))
            all_addmat_not_opened = AddMatStudent.objects.filter(opened=False, course_name=coursecurrent.course_name)
            students_addmat_not_opened_number = len(all_addmat_not_opened)
            students_addmat_not_opened_ID = []

            for student in all_addmat_not_opened:
                if str(student) not in all_addmat_not_opened:
                    students_addmat_not_opened_ID.append(str(student))

            addmat_data.append([students_addmat_not_opened_number, students_addmat_opened_number, students_addmat_not_opened_ID])

            # create dictionary for yacrs data
            students_yacrs_opened_number = len(YACRSStudent.objects.filter(accessed=True, date_accessed__range=one_week_range, course_name=coursecurrent.course_name))
            all_yacrs_not_opened = YACRSStudent.objects.filter(accessed=False, course_name=coursecurrent.course_name)
            students_yacrs_not_opened_number = len(all_yacrs_not_opened)
            students_yacrs_not_opened_ID = []

            for student in all_yacrs_not_opened:
                if str(student) not in all_yacrs_not_opened:
                    students_yacrs_not_opened_ID.append(str(student))

            yacrs_data.append([students_yacrs_not_opened_number, students_yacrs_opened_number, students_yacrs_not_opened_ID])

        result['all_courses_list'] = {'all_courses_list': all_courses_list}
        result['pres_data'] = {'data': pres_data}
        result['link_data'] = {'data': link_data}
        result['yacrs_data'] = {'data': yacrs_data}
        result['addmat_data'] = {'data': addmat_data}

    # here starts the 2nd graph displaying in time evolution of students at risk quantity
    elif request.GET['option'] == 'time_graph':

        today = dt.date.today()

        one_week_ago = today - dt.timedelta(days=7)
        one_week_range = [one_week_ago, today]

        three_months_ago = today - dt.timedelta(days=90)
        three_months_range = [three_months_ago, today]

        year_ago = today - dt.timedelta(days=365)
        year_range = [year_ago, today]

        students_at_risk_one_week = StudentCourse.objects.filter(at_risk=True, date_flagged__range=one_week_range).order_by('date_flagged')
        students_at_risk_one_week_dates, students_at_risk_one_week_numbers = [], []

        for student in students_at_risk_one_week:
            if str(student.date_flagged) not in students_at_risk_one_week_dates:
                students_at_risk_one_week_dates.append(str(student.date_flagged))
                students_at_risk_one_week_numbers.append(1)
            else:
                index = students_at_risk_one_week_dates.index(str(student.date_flagged))
                students_at_risk_one_week_numbers[index] += 1
        
        students_at_risk_three_months = StudentCourse.objects.filter(at_risk=True, date_flagged__range=three_months_range).order_by('date_flagged')
        students_at_risk_three_months_dates, students_at_risk_three_months_numbers = [], []

        for student in students_at_risk_three_months:
            if str(student.date_flagged) not in students_at_risk_three_months_dates:
                students_at_risk_three_months_dates.append(str(student.date_flagged))
                students_at_risk_three_months_numbers.append(1)
            else:
                index = students_at_risk_three_months_dates.index(str(student.date_flagged))
                students_at_risk_three_months_numbers[index] += 1

        students_at_risk_year = StudentCourse.objects.filter(at_risk=True, date_flagged__range=year_range).order_by('date_flagged')
        students_at_risk_year_dates, students_at_risk_year_numbers = [], []

        for student in students_at_risk_year:
            if str(student.date_flagged) not in students_at_risk_year_dates:
                students_at_risk_year_dates.append(str(student.date_flagged))
                students_at_risk_year_numbers.append(1)
            else:
                index = students_at_risk_year_dates.index(str(student.date_flagged))
                students_at_risk_year_numbers[index] += 1

        result['students_at_risk_one_week'] = {"dates": students_at_risk_one_week_dates, "numbers": students_at_risk_one_week_numbers}
        result['students_at_risk_three_months'] = {"dates": students_at_risk_three_months_dates, "numbers": students_at_risk_three_months_numbers}
        result['students_at_risk_year'] = {"dates": students_at_risk_year_dates, "numbers": students_at_risk_year_numbers}

    return JsonResponse(result)

# this is why pandas are used - preparation for the porgram to read from csv files if needed.
@login_required
@instructor_required
def dashboard(request):
    # read data from csv if need be - manual naming
   # file_path = os.path.join(settings.STATIC_DIR, 'data.csv')

  #  df = pd.read_csv(file_path)
   # rs = df.groupby("Engine size")["Sales in thousands"].agg("sum")
   # categories = list(rs.index)
   # values = list(rs.values)

    #table_content = df.to_html(index=None)
    #table_content = table_content.replace("", "")
    #table_content = table_content.replace('class="dataframe"', "class='table table-striped'")
    #table_content = table_content.replace('border="1"', "")

    #context_dict = {"categories": categories, 'values': values, 'table_data': table_content}
    context_dict = {}
    return render(request, 'studentsatrisk/dashboard.html', context=context_dict)


# generic signup
class SignUp(TemplateView):
    template_name = 'registration/signup.html'


# specific signup for students
class StudentSignUp(CreateView):
    model = User
    form_class = StudentForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'student'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('student_page')


#specific sign up for instructor
class InstructorSignUp(CreateView):
    model = User
    form_class = InstructorForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'instructor'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('instructor_index')


