# Generated by Django 2.2.3 on 2019-08-08 15:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('studentsatrisk', '0003_auto_20190730_2142'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='quizstudent',
            name='SID',
        ),
        migrations.RemoveField(
            model_name='quizstudent',
            name='title',
        ),
        migrations.RenameField(
            model_name='linkstudent',
            old_name='date_clicked',
            new_name='date_opened',
        ),
        migrations.RenameField(
            model_name='linkstudent',
            old_name='clicked',
            new_name='opened',
        ),
        migrations.RemoveField(
            model_name='engagement',
            name='quiz',
        ),
        migrations.DeleteModel(
            name='Quiz',
        ),
        migrations.DeleteModel(
            name='QuizStudent',
        ),
    ]
