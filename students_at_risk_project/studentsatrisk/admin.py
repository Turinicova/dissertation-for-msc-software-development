from django.contrib import admin
from studentsatrisk.models import User, School, Level, Course, Instructor,\
    CourseCurrent, Student, StudentCourse,\
    Presentation, PresStudent, Link, LinkStudent, \
    YACRS, YACRSStudent, AddMaterial, AddMatStudent


# Register your models here.

admin.site.register(User)
admin.site.register(School)
admin.site.register(Level)
admin.site.register(Course)
admin.site.register(Instructor)
admin.site.register(CourseCurrent)
admin.site.register(Student)
admin.site.register(StudentCourse)
admin.site.register(Presentation)
admin.site.register(PresStudent)
admin.site.register(Link)
admin.site.register(LinkStudent)
admin.site.register(YACRS)
admin.site.register(YACRSStudent)
admin.site.register(AddMaterial)
admin.site.register(AddMatStudent)
