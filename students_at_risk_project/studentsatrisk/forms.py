from django import forms
from django.contrib.auth.forms import UserCreationForm
from studentsatrisk.models import User, Student, Instructor


# update the existing model in the database - now the model has an attributed account based on selected already existing id for the usertype
class InstructorForm(UserCreationForm):
    IID = forms.ModelChoiceField(queryset=Instructor.objects.filter(user=None), required=True)

    class Meta(UserCreationForm.Meta):
        model = User

    def save(self):
        user = super().save(commit=False)
        user.is_instructor = True
        user.save()
        entered_IID = str(self.cleaned_data.get('IID'))
        Instructor.objects.filter(IID=entered_IID).update(user=user)

        return user


# user=none means only IDs without an attributed account will get displayed
class StudentForm(UserCreationForm):
    SID = forms.ModelChoiceField(queryset=Student.objects.filter(user=None), required=True)

    class Meta(UserCreationForm.Meta):
        model = User

    def save(self):
        user = super().save(commit=False)
        user.is_student = True
        user.save()
        entered_SID = str(self.cleaned_data.get('SID'))
        Student.objects.filter(SID=entered_SID).update(user=user)

        return user

