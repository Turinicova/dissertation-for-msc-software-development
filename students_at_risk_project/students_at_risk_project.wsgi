import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "students_at_risk_project.settings")

from django.core.wsgi import get_wsgi_application
from dj_static import Cling

application = Cling(get_wsgi_application())
